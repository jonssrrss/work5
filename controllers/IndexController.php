<?
	class IndexController
	{

		public static function action()
		{
			global $user;

			if (isset($_POST['login'])) {
				$username = $_POST['username'];
				$password = $_POST['password'];
	
				$user::login($username, $password);
			}

			if (!$user::isAuth())
				$contentFile = ROOT.'/views/content/login.php';
			else
				$contentFile = ROOT.'/views/content/main.php';

			require_once ROOT.'/lib/functions.php';
			require_once ROOT.'/views/template/index.php';
		}

		public static function actionRegister()
		{
			global $user;

			if (isset($_POST['register'])) {
				$username = $_POST['username'];
				$password = $_POST['password'];
				$password2 = $_POST['password2'];
				$fname = $_POST['fname'];
				$lname = $_POST['lname'];
				$mname = $_POST['mname'];
				$email = $_POST['email'];
	
				$user::register($fname, $lname, $mname, $email, $username, $password, $password2);
			}

			if (!$user::isAuth())
				$contentFile = ROOT.'/views/content/register.php';
			else
				header('Location: /');

			require_once ROOT.'/views/template/index.php';
		}

		public static function actionLk()
		{
			global $user;

			if (isset($_POST['update'])) {
				$username = $_POST['username'];
				$password = $_POST['password'];
				$password2 = $_POST['password2'];
				$fname = $_POST['fname'];
				$lname = $_POST['lname'];
				$mname = $_POST['mname'];
				$email = $_POST['email'];
	
				$user::update($fname, $lname, $mname, $email, $username, $password, $password2);
			}

			if ($user::isAuth()) {
				$userInfo = $user::getInfo($_SESSION['id']);
				$contentFile = ROOT.'/views/content/lk.php';
			} else
				header('Location: /');

			require_once ROOT.'/views/template/index.php';
		}
		
		public static function actionLogout()
		{
			session_destroy();
			header('Location: /');
		}

	}