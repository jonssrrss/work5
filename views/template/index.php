<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test work for work5</title>
    <link rel="stylesheet" href="/assets/css/style.css?v=1">
</head>
<body>

    <div class="wrapper">
        <header>
            <? if ($user::isAuth()) {
                echo '<a href="/">Главная</a><div><a href="/lk">Личный кабинет</a><a href="/logout">Выход</a></div>';
            } ?>
        </header>
        <? require_once($contentFile); ?>
    </div>

</body>
</html>