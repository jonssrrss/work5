<div class="content">

<?

$query1 = "SELECT
    email
FROM
    users
GROUP BY
    email
HAVING
    COUNT(*) > 1"
;


$query2 = "SELECT
    login
FROM
    `users`
WHERE
    `users`.`id` NOT IN(
        SELECT
            user_id
        FROM
            `orders`
        GROUP BY
            user_id
    )"
;


$query3 = "SELECT
    u.login
FROM
    `orders` o
    LEFT JOIN `users` u ON u.id = o.user_id
GROUP BY
    user_id
HAVING
    COUNT(*) > 2"
;

?>

    <p class="info">Список email'лов встречающихся более чем у одного пользователя:</p>

    <pre class="code"><?=$query1;?></pre>

    <div class="info result">Результат: <?=getTable($query1);?></div>


    <p class="info">Список логинов пользователей, которые не сделали ни одного заказа:</p>

    <pre class="code"><?=$query2;?></pre>

    <div class="info result">Результат: <?=getTable($query2);?></div>


    <p class="info">Список логинов пользователей которые сделали более двух заказов:</p>

    <pre class="code"><?=$query3;?></pre>

    <div class="info result">Результат: <?=getTable($query3);?></div>


    
    

</div>