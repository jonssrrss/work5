<?
    function getTable($query)
    {
        global $db;

        $result = $db->query($query);

        $tableHTML = '<table border="1px">';

        foreach ($result as $key => $value) {
            $tableHTML .= '<tr>';
            foreach ($value as $key2 => $value2) {
                $tableHTML .= '<td>';
                $tableHTML .= $value2;
                $tableHTML .= '</td>';
            }
            $tableHTML .= '</tr>';
        }

        $tableHTML .= '</table>';

        return $tableHTML;
    }