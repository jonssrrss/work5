<?

    /**
     * Работа с пользователем
     */
    class User
    {

        /**
         * Проверка авторизованности пользователя
         */
        public static function isAuth()
        {
            return isset($_SESSION['id']);
        }

        /**
         * Авторизация
         */
        public static function login($username, $password)
        {
            global $db;

            $query = "
                SELECT
                    *
                FROM
                    `users`
                WHERE
                    `login` = '" . $username . "' AND
                    `password` = '" . md5($password) . "'
            ";

            $result = $db->query($query);

            if (count($result) > 0) {
                $_SESSION['id'] = $result[0]['id'];
                return true;
            }

            return false;
        }

        /**
         * Регистрация
         */
        public static function register($fname, $lname, $mname, $email, $username, $password, $password2)
        {
            global $db;

            if ($password != $password2) {
                return false;
            }

            $query = "
                INSERT INTO
                    `users`
                (
                    `fname`,
                    `lname`,
                    `mname`,
                    `email`,
                    `login`,
                    `password`
                )
                VALUES
                (
                    '" . $fname . "',
                    '" . $lname . "',
                    '" . $mname . "',
                    '" . $email . "',
                    '" . $username . "',
                    '" . md5($password) . "'
                );
            ";

            $id = $db->insert($query);

            $_SESSION['id'] = $id;

            return true;
        }

        /**
         * Обновление информации
         */
        public static function update($fname, $lname, $mname, $email, $username, $password, $password2)
        {
            global $db;

            if ($password != $password2) {
                return false;
            }

            $query = "
                UPDATE
                    `users`
                SET
                    `fname` = '" . $fname . "',
                    `lname` = '" . $lname . "',
                    `mname` = '" . $mname . "',
                    `email` = '" . $email . "',
                    `login` = '" . $username . "',
                    `password` = '" . md5($password) . "'
                WHERE
                    `users`.`id` = " . $_SESSION['id'] . ";
            ";
            
            $result = $db->query($query);

            return true;
        }

        /**
         * Получение информации о пользователе
         */
        public static function getInfo($id)
        {
            global $db;

            $query = "
                SELECT
                    *
                FROM
                    `users`
                WHERE
                    `users`.`id` = " . $id . "
            ";

            $result = $db->query($query);

            return $result[0];
        }
        
    }